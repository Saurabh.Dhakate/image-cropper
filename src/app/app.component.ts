import { Component, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CropperComponent, ImageCropperResult } from 'angular-cropperjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'image-crop';
  @ViewChild('angularCropper') public angularCropper: any;
  config = [];
  imageUrl: any;

  resultImage: any;
  resultResult: any;

  constructor(
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {

  }

  Crop() {
    this.angularCropper.exportCanvas();
  }

  resultImageFun() {
    this.resultResult = this.angularCropper.cropper.getCroppedCanvas().toDataURL('image/jpeg');
  }

  checkstatus(event: any) {
    console.log(event.blob);
    if (event.blob === undefined) {
      return;
    }
    let urlCreator = window.URL;
    this.resultResult = this.sanitizer.bypassSecurityTrustUrl(
      urlCreator.createObjectURL(new Blob(event.blob)));
  }

  file: any;

  readURL(event: any): void {
    console.log(event)
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      this.file = event.target.files[0];
      reader.onload = (e) => (this.imageUrl = reader.result);
      console.log(this.imageUrl)
      reader.readAsDataURL(this.file);
    }
    event.target.value = ''
  }
}
